package com.martjinni.delmedApp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.martjinni.Appdelmed.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class orderDetails extends AppCompatActivity {
    String returnPage;
    RecyclerView displayItem;
    TextView orderIdView, customerName, phoneNumber, orderStatus, addLine1, addLine2, addLine3, totalItemCount;
    int orderId;
    Context mCtx;
    volleyClass newClass = new volleyClass ();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.activity_order_details);
        SharedPreferences sharedPreferences = (getApplicationContext()).getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        if(sharedPreferences.getString ("sessionInfo","").equals ("")){
            Intent intent = new Intent(this, login.class);
            startActivity(intent);
            finish();
        }
        else{
            mCtx = this;
            returnPage = (getIntent ().getStringExtra ("fromTab"));
            workWithInformation runnable = new workWithInformation ();
            new Thread (runnable).start ();
            orderId = Integer.valueOf (getIntent ().getStringExtra ("orderId"));
            orderIdView = findViewById (R.id.orderId);
            customerName = findViewById (R.id.customerName);
            phoneNumber = findViewById (R.id.phoneNumber);
            orderStatus = findViewById (R.id.orderStatus);
            addLine1 = findViewById (R.id.addLine1);
            addLine2 = findViewById (R.id.addLine2);
            addLine3 = findViewById (R.id.addLine3);
            totalItemCount = findViewById (R.id.itemCount);
            displayItem = findViewById (R.id.displayShopItems);
            displayItem.setHasFixedSize (false);
            displayItem.setNestedScrollingEnabled (false);
            displayItem.setLayoutManager (new LinearLayoutManager (this));
        }
    }

    class workWithInformation implements Runnable{

        @Override
        public void run(){
            SharedPreferences sharedPreferences = (getApplicationContext()).getSharedPreferences("userInfo", Context.MODE_PRIVATE);
            try{
                Cache cache = new DiskBasedCache (getCacheDir(), 1024 * 1024);
                Network network = new BasicNetwork (new HurlStack ());
                RequestQueue mRequestQueue = new RequestQueue (cache, network);
                mRequestQueue.start();
                final JSONObject sessionInfo = new JSONObject (sharedPreferences.getString ("sessionInfo",""));
                JSONObject requestObject = new JSONObject ();
                requestObject.put ("typeId",sessionInfo.getInt ("typeId"));
                requestObject.put ("userId",sessionInfo.getInt ("userId"));
                requestObject.put ("userType",sessionInfo.getString ("userType"));
                requestObject.put ("orderId",String.valueOf (orderId));
                String finalUrl = newClass.baseUrl+"orderdetails";
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest (Request.Method.POST, finalUrl, requestObject,  new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        displayInformation (response);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace ();
                    }
                }){
                    @Override
                    public Map getHeaders(){
                        Map<String,String> params = new HashMap<> ();
                        params.put("Content-Type", "application/json");
                        try{
                            params.put("basicAuthenticate", sessionInfo.getString ("basicAuthenticate"));
                        }
                        catch (JSONException jsonExcepion2){
                            jsonExcepion2.printStackTrace ();
                        }
                        return params;
                    }
                };
                mRequestQueue.add(jsonObjectRequest);
            }
            catch (JSONException jsonException){
                jsonException.printStackTrace ();
            }
        }

        private void displayInformation(final JSONObject response){
            try{
                runOnUiThread (new Runnable () {
                    @Override
                    public void run() {
                        try{
                            String orderInfo = "Order Id :"+orderId;
                            orderIdView.setText (String.valueOf (orderInfo));
                            customerName.setText (response.getString ("custName"));
                            phoneNumber.setText (String.valueOf (response.getInt ("custNumber")));
                            orderStatus.setText (returnPage);
                            String addLine3User = response.getString ("city")+" , "+response.getString ("state")+" , "+response.getString ("country");
                            addLine3.setText (addLine3User);
                            addLine2.setText (response.getString ("addLine1"));
                            addLine1.setText (response.getString ("addLine2"));
                            int totalCountUse = response.getJSONArray ("itemList").length ();
                            totalItemCount.setText (String.valueOf (totalCountUse));
                        }
                        catch (JSONException jsonException){
                            jsonException.printStackTrace ();
                        }
                    }
                });
                JSONArray itemList = response.getJSONArray ("itemList");
                List<viewClassOrderDetailsItem> userInfo = new ArrayList<> ();
                for(int i =0;i<itemList.length ();i++){
                    JSONObject interimObject = itemList.getJSONObject (i);
                    int displayAddon = 1;
                    if(interimObject.getString ("addOnName").equals ("")){
                        displayAddon = 0;
                    }
                    String[] itemNameInfo = interimObject.getString ("itemName").split ("@");
                    String displayPrice = "Rs . "+interimObject.getInt ("value");
                    userInfo.add (new viewClassOrderDetailsItem (itemNameInfo[0],itemNameInfo[1],displayPrice,interimObject.getString ("addOnName"),displayAddon));
                }
                final viewAdapterOrderDetailsItem adapter = new viewAdapterOrderDetailsItem (mCtx,userInfo);
                runOnUiThread (new Runnable () {
                    @Override
                    public void run() {
                        displayItem.setAdapter (adapter);
                    }
                });
            }
            catch (JSONException jsonException){
                jsonException.printStackTrace ();
            }
        }
    }

    public void backPressed(View view){
        goToOrders ();
    }

    public void goToOrders(){
        Intent intent = new Intent(this, orders.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed()
    {
        goToOrders ();
        super.onBackPressed();  // optional depending on your needs
    }
}
