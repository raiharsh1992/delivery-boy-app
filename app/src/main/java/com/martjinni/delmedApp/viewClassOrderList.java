package com.martjinni.delmedApp;

import org.json.JSONArray;

public class viewClassOrderList {
    private String cutomerName,orderStatus;
    private int orderId, phoneNumber;
    private JSONArray itemList;

    public viewClassOrderList(String cutomerName, String orderStatus, int orderId, int phoneNumber, JSONArray itemList) {
        this.cutomerName = cutomerName;
        this.orderStatus = orderStatus;
        this.orderId = orderId;
        this.phoneNumber = phoneNumber;
        this.itemList = itemList;
    }

    public String getCutomerName() {
        return cutomerName;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public int getOrderId() {
        return orderId;
    }

    public int getPhoneNumber() {
        return phoneNumber;
    }

    public JSONArray getItemList() {
        return itemList;
    }
}
