package com.martjinni.delmedApp;

public class viewClassOrderDetailsItem {
    private String itemName, quantity, totalCost, addOnName;
    private int displayAddOn;

    public viewClassOrderDetailsItem(String itemName, String quantity, String totalCost, String addOnName, int displayAddOn) {
        this.itemName = itemName;
        this.quantity = quantity;
        this.totalCost = totalCost;
        this.addOnName = addOnName;
        this.displayAddOn = displayAddOn;
    }

    public String getItemName() {
        return itemName;
    }

    public String getQuantity() {
        return quantity;
    }

    public String getTotalCost() {
        return totalCost;
    }

    public String getAddOnName() {
        return addOnName;
    }

    public int getDisplayAddOn() {
        return displayAddOn;
    }
}
