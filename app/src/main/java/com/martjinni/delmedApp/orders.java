package com.martjinni.delmedApp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.darwindeveloper.horizontalscrollmenulibrary.custom_views.HorizontalScrollMenuView;
import com.darwindeveloper.horizontalscrollmenulibrary.extras.MenuItem;
import com.martjinni.Appdelmed.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class orders extends AppCompatActivity {
    HorizontalScrollMenuView menu;
    TextView scrollHeader;
    android.support.v7.widget.Toolbar myToolbar;
    ImageView scrollImage;
    private volatile boolean stopThread = true;
    private volatile int displayPosition=0;
    private volatile boolean workingOnAssign = true;
    RecyclerView displayOrderList;
    volleyClass newClass = new volleyClass ();
    LinearLayout displayNoItem, displayOrderInfo;
    Context mCtx;
    viewAdapterOrderList adapter;
    List<viewClassOrderList> userInfo = new ArrayList<> ();
    Cache cache;
    Network network;
    RequestQueue mRequestQueue;
    String viewType;
    private volatile JSONArray itemList = new JSONArray ();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cache = new DiskBasedCache (getCacheDir(), 1024 * 1024);
        network = new BasicNetwork (new HurlStack ());
        mRequestQueue = new RequestQueue (cache, network);
        mRequestQueue.start();
        SharedPreferences sharedPreferences = (getApplicationContext()).getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        setContentView(R.layout.activity_orders);
        scrollHeader = findViewById (R.id.scrollHeader);
        scrollImage = findViewById (R.id.scrollImage);
        myToolbar = findViewById (R.id.my_toolbar);
        displayOrderList = findViewById (R.id.displayOrderList);
        displayNoItem = findViewById (R.id.displayNoItem);
        displayOrderInfo = findViewById (R.id.displayScrollView);
        displayOrderList.setHasFixedSize (false);
        displayOrderList.setNestedScrollingEnabled (false);
        displayOrderList.setLayoutManager (new LinearLayoutManager (mCtx));
        initMenu ();
        try{
            JSONObject sessionInfo = new JSONObject (sharedPreferences.getString ("sessionInfo",""));
            myToolbar.setTitle (sessionInfo.getString ("userName"));
        }
        catch (JSONException jsonException){
            jsonException.printStackTrace ();
        }
        setSupportActionBar (myToolbar);
        getSupportActionBar ().setDisplayHomeAsUpEnabled(false);
        mCtx = this;
        if(sharedPreferences.getString ("sessionInfo","").equals ("")){
            Intent intent = new Intent(this, login.class);
            startActivity(intent);
            finish();
        }
        else{
            displayOrderThread runnable = new displayOrderThread ("NEW");
            viewType = "NEW";
            new Thread (runnable).start ();
            stopThread = false;
            workingOnAssign = false;
        }
    }

    public boolean onCreateOptionsMenu(Menu menuUse){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menuoption, menuUse);
        return true;
    }

    public void initMenu(){
        menu = findViewById(R.id.menu);
        menu.addItem("NEW ORDERS",R.drawable.ic_add_alarm_black_24dp, true);
        menu.addItem("IN PROGRESS",R.drawable.ic_hourglass_empty_black_24dp, false);
        menu.addItem("COMPLETED",R.drawable.ic_check_circle_black_24dp, false);
        menu.setOnHSMenuClickListener (new HorizontalScrollMenuView.OnHSMenuClickListener () {
            @Override
            public void onHSMClick(MenuItem menuItem, int position) {
                if(stopThread&&workingOnAssign){
                    menu.setItemSelected (position);
                    if(position==0){
                        displayOrderThread runnable = new displayOrderThread ("NEW");
                        new Thread (runnable).start ();
                        scrollHeader.setText ("New Orders.");
                        scrollImage.setImageResource (R.drawable.group_2);
                        displayPosition = 0;
                        stopThread = false;
                        workingOnAssign = false;
                        viewType = "NEW";
                    }
                    else if(position==1){
                        displayOrderThread runnable = new displayOrderThread ("INPROGRESS");
                        new Thread (runnable).start ();
                        scrollHeader.setText ("In progress.");
                        scrollImage.setImageResource (R.drawable.icons_8_in_transit_64);
                        displayPosition = 1;
                        stopThread = false;
                        workingOnAssign = false;
                        viewType = "INPROGRESS";
                    }
                    else{
                        displayOrderThread runnable = new displayOrderThread ("COMPLETED");
                        new Thread (runnable).start ();
                        scrollHeader.setText ("Completed orders.");
                        scrollImage.setImageResource (R.drawable.icons_8_delivery_96);
                        displayPosition = 1;
                        stopThread = false;
                        workingOnAssign = false;
                        viewType = "COMPLETED";
                    }
                }
                else if(!stopThread){
                    if(displayPosition==0){
                        Toast.makeText (getApplicationContext (), "Currently loading New orders", Toast.LENGTH_SHORT).show ();
                    }
                    else if(displayPosition==1){
                        Toast.makeText (getApplicationContext (), "Currently loading Working orders", Toast.LENGTH_SHORT).show ();
                    }
                    else{
                        Toast.makeText (getApplicationContext (), "Currently loading Completed orders", Toast.LENGTH_SHORT).show ();
                    }
                }
                else{
                    Toast.makeText (getApplicationContext (), "Currently assigning orders", Toast.LENGTH_SHORT).show ();
                }
            }
        });
    }

    class displayOrderThread implements Runnable{
        String viewType;
        displayOrderThread(String viewType){
            this.viewType = viewType;
        }
        @Override
        public void run(){
            try{
                SharedPreferences sharedPreferences = (getApplicationContext()).getSharedPreferences("userInfo", Context.MODE_PRIVATE);
                final JSONObject sessionInfo = new JSONObject (sharedPreferences.getString ("sessionInfo",""));
                JSONObject requestObject = new JSONObject ();
                requestObject.put ("userType",sessionInfo.getString ("userType"));
                requestObject.put ("typeId",sessionInfo.getInt ("typeId"));
                requestObject.put ("userId",sessionInfo.getInt ("userId"));
                requestObject.put ("viewType",viewType);
                String finalUrl = newClass.baseUrl+"orderlist";
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest (Request.Method.POST, finalUrl, requestObject,  new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        displayInformation (response);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace ();
                        displayOrderInfo.setVisibility (View.INVISIBLE);
                        displayNoItem.setVisibility (View.VISIBLE);
                        stopThread = true;
                        workingOnAssign = true;
                    }
                }){
                    @Override
                    public Map getHeaders() throws AuthFailureError {
                        Map<String,String> params = new HashMap<> ();
                        params.put("Content-Type", "application/json");
                        try{
                            params.put("basicAuthenticate", sessionInfo.getString ("basicAuthenticate"));
                        }
                        catch (JSONException jsonExcepion2){
                            jsonExcepion2.printStackTrace ();
                        }
                        return params;
                    }
                };
                mRequestQueue.add(jsonObjectRequest);

            }
            catch (JSONException jsonException){
                jsonException.printStackTrace ();
            }
        }

        public void displayInformation(JSONObject response){
            try{
                userInfo = new ArrayList<> ();
                if(response.getInt ("totalCount")>0){
                    displayNoItem.setVisibility (View.GONE);
                    displayOrderInfo.setVisibility (View.VISIBLE);
                    JSONArray dataSet = response.getJSONArray ("data");
                    for(int i=0;i<dataSet.length ();i++){
                        JSONObject interimObject = dataSet.getJSONObject (i);
                        userInfo.add (new viewClassOrderList (interimObject.getString ("custName"),viewType,interimObject.getInt ("orderStoreId"),interimObject.getInt ("phoneNumber"),interimObject.getJSONArray ("itemList")));
                    }
                    adapter = new viewAdapterOrderList (mCtx,userInfo);
                    runOnUiThread (new Runnable () {
                        @Override
                        public void run() {
                            displayOrderList.setAdapter (adapter);
                        }
                    });
                    stopThread = true;
                    workingOnAssign = true;
                }
                else{
                    runOnUiThread (new Runnable () {
                        @Override
                        public void run() {
                            displayNoItem.setVisibility (View.VISIBLE);
                            displayOrderInfo.setVisibility (View.INVISIBLE);
                        }
                    });
                    stopThread = true;
                    workingOnAssign = true;
                }
            }
            catch (JSONException jsonException){
                jsonException.printStackTrace ();
            }
        }
    }

    public void goToNext(int orderId){
        Intent intent = new Intent(this, orderDetails.class);
        intent.putExtra("orderId",String.valueOf (orderId));
        intent.putExtra("fromTab",viewType);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(android.view.MenuItem item){
        if(item.getItemId ()==R.id.new_activity){
            AlertDialog.Builder mBuilder = new AlertDialog.Builder(this);
            View mView = getLayoutInflater().inflate(R.layout.alertdialog, null);
            mBuilder.setView(mView);
            final AlertDialog dialog = mBuilder.create();
            TextView errorMessage = mView.findViewById (R.id.errormessage);
            String displayMessage = "Do you wish to Logout?";
            errorMessage.setText (displayMessage);
            TextView continueView = mView.findViewById (R.id.modalclose);
            continueView.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    dialog.dismiss ();
                    logoutFinal ();
                }
            });
            TextView cancelView = mView.findViewById (R.id.modalContinue);
            cancelView.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    dialog.dismiss ();
                }
            });
            dialog.show ();
        }
        return true;
    }
    public void logoutFinal(){
        disableNotif ();
    }

    public void systemLogOut(){
        final Context mCtx = this;
        try{
            final SharedPreferences sharedPreferences = (getApplicationContext()).getSharedPreferences("userInfo", Context.MODE_PRIVATE);
            final JSONObject sessionInfo = new JSONObject (sharedPreferences.getString ("sessionInfo",""));
            JSONObject requestObject = new JSONObject ();
            requestObject.put("userType",sessionInfo.getString ("userType"));
            requestObject.put ("userId",sessionInfo.getInt ("userId"));
            Cache cache = new DiskBasedCache (getCacheDir(), 1024 * 1024);
            Network network = new BasicNetwork (new HurlStack ());
            RequestQueue mRequestQueue = new RequestQueue (cache, network);
            mRequestQueue.start();
            String finalUrl = newClass.baseUrl+"signout";
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest (Request.Method.POST, finalUrl, requestObject,  new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    sharedPreferences.edit ().remove ("sessionInfo").apply ();
                    Intent intent = new Intent(mCtx, orderDetails.class);
                    startActivity(intent);
                    finish ();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    sharedPreferences.edit ().remove ("sessionInfo").apply ();
                    Intent intent = new Intent(mCtx, orderDetails.class);
                    startActivity(intent);
                    finish ();
                }
            }){
                @Override
                public Map getHeaders(){
                    Map<String,String> params = new HashMap<> ();
                    params.put("Content-Type", "application/json");
                    try{
                        params.put("basicAuthenticate", sessionInfo.getString ("basicAuthenticate"));
                    }
                    catch (JSONException jsonExcepion2){
                        jsonExcepion2.printStackTrace ();
                    }
                    return params;
                }
            };
            mRequestQueue.add(jsonObjectRequest);
        }
        catch (JSONException jsonException){
            jsonException.printStackTrace ();
        }
    }

    public void exit(){
        android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(1);
    }

    @Override
    public void onBackPressed()
    {
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(this);
        View mView = getLayoutInflater().inflate(R.layout.alertdialog, null);
        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.create();
        TextView errorMessage = mView.findViewById (R.id.errormessage);
        String displayMessage = "Do you wish to exit?";
        errorMessage.setText (displayMessage);
        TextView continueView = mView.findViewById (R.id.modalclose);
        continueView.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                dialog.dismiss ();
                exit ();
            }
        });
        TextView cancelView = mView.findViewById (R.id.modalContinue);
        cancelView.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                dialog.dismiss ();
            }
        });
        dialog.show ();
    }
    public void updateStatus(viewClassOrderList container){
        String orderNewStatus = "PICKED";
        if(container.getOrderStatus ().equals ("INPROGRESS")){
            orderNewStatus = "DELIVER";
        }
        workingOnAssign = false;
        updatingStatus runner = new updatingStatus(container.getItemList (),orderNewStatus,container.getOrderId ());
        new Thread (runner).start ();
    }

    class updatingStatus implements Runnable{
        JSONArray itemList;
        String orderStatus;
        int orderId;

        public updatingStatus(JSONArray itemList, String orderStatus, int orderId) {
            this.itemList = itemList;
            this.orderStatus = orderStatus;
            this.orderId = orderId;
        }

        @Override
        public void run(){
            try{
                SharedPreferences sharedPreferences = (getApplicationContext()).getSharedPreferences("userInfo", Context.MODE_PRIVATE);
                final JSONObject sessionInfo = new JSONObject (sharedPreferences.getString ("sessionInfo",""));
                JSONObject requestObject = new JSONObject ();
                requestObject.put ("userType",sessionInfo.getString ("userType"));
                requestObject.put ("typeId",sessionInfo.getInt ("typeId"));
                requestObject.put ("userId",sessionInfo.getInt ("userId"));
                requestObject.put ("orderStatus",orderStatus);
                requestObject.put ("itemBeingMarked",itemList);
                requestObject.put ("orderStoreId",orderId);
                String finalUrl = newClass.baseUrl+"updateorderstatus";
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest (Request.Method.POST, finalUrl, requestObject,  new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        displayOrderThread runnable = new displayOrderThread (viewType);
                        new Thread (runnable).start ();
                        runOnUiThread (new Runnable () {
                            @Override
                            public void run() {
                                Toast.makeText (getApplicationContext (), "Order updated", Toast.LENGTH_SHORT).show ();
                            }
                        });
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        displayOrderThread runnable = new displayOrderThread (viewType);
                        new Thread (runnable).start ();
                        runOnUiThread (new Runnable () {
                            @Override
                            public void run() {
                                Toast.makeText (getApplicationContext (), "Issue while updating order", Toast.LENGTH_SHORT).show ();
                            }
                        });
                    }
                }){
                    @Override
                    public Map getHeaders() throws AuthFailureError {
                        Map<String,String> params = new HashMap<> ();
                        params.put("Content-Type", "application/json");
                        try{
                            params.put("basicAuthenticate", sessionInfo.getString ("basicAuthenticate"));
                        }
                        catch (JSONException jsonExcepion2){
                            jsonExcepion2.printStackTrace ();
                        }
                        return params;
                    }
                };
                mRequestQueue.add(jsonObjectRequest);

            }
            catch (JSONException jsonException){
                jsonException.printStackTrace ();
            }
        }
    }

    public void disableNotif(){
        try{
            SharedPreferences sharedPreferences = (getApplicationContext()).getSharedPreferences("userInfo", Context.MODE_PRIVATE);
            if(!sharedPreferences.getString ("sessionInfo","").equals ("")){
                final JSONObject sessionInfo = new JSONObject (sharedPreferences.getString ("sessionInfo",""));
                JSONObject requestObject = new JSONObject ();
                String token = sharedPreferences.getString ("userMessageToken","");
                if(!token.equals ("")){
                    requestObject.put ("userType",sessionInfo.getString ("userType"));
                    requestObject.put ("typeId",sessionInfo.getInt ("typeId"));
                    requestObject.put ("userId",sessionInfo.getInt ("userId"));
                    requestObject.put ("token",token);
                    Cache cache = new DiskBasedCache (getCacheDir(), 1024 * 1024);
                    Network network = new BasicNetwork (new HurlStack ());
                    RequestQueue mRequestQueue = new RequestQueue (cache, network);
                    mRequestQueue.start();
                    String finalUrl = newClass.baseUrl+"remusertoken";
                    JsonObjectRequest jsonObjectRequest = new JsonObjectRequest (Request.Method.POST, finalUrl, requestObject,  new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            systemLogOut ();
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            systemLogOut ();
                            error.printStackTrace ();
                        }
                    }){
                        @Override
                        public Map getHeaders() throws AuthFailureError {
                            Map<String,String> params = new HashMap<> ();
                            params.put("Content-Type", "application/json");
                            try{
                                params.put("basicAuthenticate", sessionInfo.getString ("basicAuthenticate"));
                            }
                            catch (JSONException jsonExcepion2){
                                systemLogOut ();
                                jsonExcepion2.printStackTrace ();
                            }
                            return params;
                        }
                    };
                    mRequestQueue.add(jsonObjectRequest);
                }
                else{
                    systemLogOut ();
                }
            }
        }
        catch (JSONException jsonException){
            systemLogOut ();
            jsonException.printStackTrace ();
        }
    }
}
