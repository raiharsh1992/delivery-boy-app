package com.martjinni.delmedApp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.martjinni.Appdelmed.R;

import java.util.List;

public class viewAdapterOrderList extends RecyclerView.Adapter<viewAdapterOrderList.viewHolderOrderList>{

    private Context mCtx;
    private List<viewClassOrderList> infoUsed;

    public viewAdapterOrderList(Context mCtx, List<viewClassOrderList> infoUsed) {
        this.mCtx = mCtx;
        this.infoUsed = infoUsed;
    }

    @NonNull
    @Override
    public viewHolderOrderList onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from (mCtx);
        View view = inflater.inflate (R.layout.order_list_layout,null);
        return new viewHolderOrderList (view);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolderOrderList holder, int position) {
        viewClassOrderList container = infoUsed.get (position);
        holder.custName.setText (container.getCutomerName ());
        holder.orderStatus.setText (container.getOrderStatus ());
        holder.phoneNumber.setText (String.valueOf (container.getPhoneNumber ()));
        String OrderId = "Order Id :"+container.getOrderId ();
        holder.orderId.setText (OrderId);
        if(container.getOrderStatus ().equals ("COMPLETED")){
            holder.assignOrder.setVisibility (View.GONE);
        }
        else if(container.getOrderStatus ().equals ("INPROGRESS")){
            holder.assignOrder.setText ("DELIVER");
            holder.assignOrder.setVisibility (View.VISIBLE);
        }
        else{
            holder.assignOrder.setText ("PICKUP");
            holder.assignOrder.setVisibility (View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return infoUsed.size ();
    }


    class viewHolderOrderList extends RecyclerView.ViewHolder{
        TextView custName, orderId, phoneNumber, orderStatus, viewDetails,assignOrder;
        public viewHolderOrderList(View itemView){
            super(itemView);
            custName = itemView.findViewById (R.id.customerName);
            orderStatus = itemView.findViewById (R.id.orderStatus);
            viewDetails = itemView.findViewById (R.id.orderDetails);
            orderId = itemView.findViewById (R.id.orderId);
            phoneNumber = itemView.findViewById (R.id.totalAmount);
            assignOrder = itemView.findViewById (R.id.assignOrder);
            assignOrder.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition ();
                    viewClassOrderList container = infoUsed.get (position);
                    ((orders)mCtx).updateStatus (container);
                }
            });
            viewDetails.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition ();
                    viewClassOrderList container = infoUsed.get (position);
                    ((orders)mCtx).goToNext (container.getOrderId ());
                }
            });
        }
    }
}
